# MapProxy

[MapProxy](https://mapproxy.org) is an [open source](https://github.com/mapproxy/mapproxy) proxy for geospatial data. It caches, accelerates and transforms data from existing map services.

Openstreetcraft uses MapProxy as full compliant [WMS](https://en.wikipedia.org/wiki/Web_Map_Service) server to have an unified interface to different geospatial data sources.

# Installation

MapProxy is deployed as Docker container. A test server can be started with

```
make start
```

MapProxy comes with a demo service that lists all configured WMS and TMS layers.
You can access that service at http://localhost:8080/demo/

# Data sources

## Terrain data

https://registry.opendata.aws/terrain-tiles/ is uses as source for terrain tiles.
 
DEM is available in layer `EARTH:ELEVATION` as 
[Terrarium](https://mapzen.com/documentation/terrain-tiles/formats/#terrarium) formatted PNG.
 
## Biomes

[OSM Water Polygons](https://osmdata.openstreetmap.de/data/water-polygons.html) data set.
 
Ocean water body is available in layer `EARTH:OCEAN`.

## Earth coverage

[ESA Globcover](http://due.esrin.esa.int/page_globcover.php) data set.
 
Coverage data is available in layer `EARTH:COVER`.

## Plastic pollution

[Plastic pollution](https://gitlab.com/openstreetcraft/mapserver-data#plastic-pollution) dataset.
 
Plastic pollution data is available in layers `EARTH:PLASTIC_C1` to `EARTH:PLASTIC_C4`.

 