# Copyright (C) 2017  Gerald Fiedler
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM httpd:2.4.48

RUN apt-get update \
 && apt-get install -y \
      libapache2-mod-wsgi \
      libjpeg-dev \
      libtiff-dev \
      libfreetype6-dev \
      libyaml-dev \
      zlib1g-dev \
      libpng-dev \
      python-dev \
      python-pil \
      python-pip \
      python-pyproj \
      python-yaml \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade --force-reinstall MapProxy==1.11.1 Pillow

EXPOSE 80

COPY apache/httpd.conf /usr/local/apache2/conf/httpd.conf
COPY mapproxy /mapproxy
