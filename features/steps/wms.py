import os
import requests
import tempfile
import time
from behave import given, when, then
from hamcrest import assert_that, equal_to
from retrying import retry
from subprocess import check_output


@given(u'a started Mapproxy server')
@retry(stop_max_delay=10000)
def step_impl(context):
    check_output(['docker-compose', 'exec', '-T', 'mapproxy', 'echo', 'alive'])
    # wait for linked containers to be up
    check_output(['docker-compose', 'exec', '-T', 'mapserver', 'echo', 'alive'])
    check_output(['docker-compose', 'exec', '-T', 'tms', 'echo', 'alive'])
    # wait for linked containers to be ready
    time.sleep(10) 


@when(u'I send the WMS GetMap request')
def step_impl(context):
    url = 'http://{host}:8080/service'.format(host=os.getenv('DOCKERHOST', 'localhost'))
    params = {
        'SERVICE': 'WMS',
        'VERSION': '1.1.1',
        'REQUEST': 'GetMap',
        }
    for row in context.table:
        params[row['parameter']] = row['value']
    context.response = requests.get(url, params=params)
    assert context.response.ok


@then(u'I receive the image')
def step_impl(context):
    with tempfile.NamedTemporaryFile() as file:
        file.write(context.response.content)
        file.flush()
        try:
            output = check_output(['convert', file.name, 'txt:-'])
        except:
            assert False, context.response.content
        assert_that(output.decode('UTF-8').strip(), equal_to(context.text), context.response.text)
    
