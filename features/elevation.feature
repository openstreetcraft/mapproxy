Feature: Earth elevation tiles

  As a WMS client,
  I want to use AWS elevation tiles

  Scenario: Receiving image from resampled AWS elevation tile layer
    Given a started Mapproxy server 
    When I send the WMS GetMap request
      | parameter | value           |
      | LAYERS    | EARTH:ELEVATION |
      | STYLES    |                 |
      | SRS       | EPSG:3857       |
      | FORMAT    | image/png       |
      | BBOX      | 10,10,10.4,10.4 |
      | WIDTH     | 4               |
      | HEIGHT    | 4               |
    Then I receive the image
      """
      # ImageMagick pixel enumeration: 4,4,255,srgb
      0,0: (77,169,115)  #4DA973  srgb(77,169,115)
      1,0: (77,169,115)  #4DA973  srgb(77,169,115)
      2,0: (77,169,115)  #4DA973  srgb(77,169,115)
      3,0: (77,169,115)  #4DA973  srgb(77,169,115)
      0,1: (77,169,115)  #4DA973  srgb(77,169,115)
      1,1: (77,169,115)  #4DA973  srgb(77,169,115)
      2,1: (77,169,115)  #4DA973  srgb(77,169,115)
      3,1: (77,169,115)  #4DA973  srgb(77,169,115)
      0,2: (77,169,115)  #4DA973  srgb(77,169,115)
      1,2: (77,169,115)  #4DA973  srgb(77,169,115)
      2,2: (77,169,115)  #4DA973  srgb(77,169,115)
      3,2: (77,169,115)  #4DA973  srgb(77,169,115)
      0,3: (77,169,115)  #4DA973  srgb(77,169,115)
      1,3: (77,169,115)  #4DA973  srgb(77,169,115)
      2,3: (77,169,115)  #4DA973  srgb(77,169,115)
      3,3: (77,169,115)  #4DA973  srgb(77,169,115)
      """
