Feature: plastic pollution data

  As a WMS client,
  I want to use plastic pollution data

  Scenario: Receiving image from plastic pollution layer
    Given a started Mapproxy server 
    When I send the WMS GetMap request
      | parameter | value                   |
      | LAYERS    | EARTH:PLASTIC_C4        |
      | STYLES    |                         |
      | SRS       | EPSG:4326               |
      | FORMAT    | image/png               |
      | BBOX      | -150.1,30.0,-150.0,30.1 |
      | WIDTH     | 4                       |
      | HEIGHT    | 4                       |
    Then I receive the image
      """
      # ImageMagick pixel enumeration: 4,4,255,srgb
      0,0: (3,3,3)  #030303  grey1
      1,0: (3,3,3)  #030303  grey1
      2,0: (3,3,3)  #030303  grey1
      3,0: (3,3,3)  #030303  grey1
      0,1: (3,3,3)  #030303  grey1
      1,1: (3,3,3)  #030303  grey1
      2,1: (3,3,3)  #030303  grey1
      3,1: (3,3,3)  #030303  grey1
      0,2: (3,3,3)  #030303  grey1
      1,2: (3,3,3)  #030303  grey1
      2,2: (3,3,3)  #030303  grey1
      3,2: (3,3,3)  #030303  grey1
      0,3: (3,3,3)  #030303  grey1
      1,3: (3,3,3)  #030303  grey1
      2,3: (3,3,3)  #030303  grey1
      3,3: (3,3,3)  #030303  grey1
      """
