import subprocess


def before_all(context):
    subprocess.check_output(['make', 'start'])


def after_all(context):
    subprocess.check_output(['make', 'stop'])
