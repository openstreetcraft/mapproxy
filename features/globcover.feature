Feature: GLOBCOVER data

  As a WMS client,
  I want to use GLOBCOVER data

  Scenario: Receiving image from GLOBCOVER layer
    Given a started Mapproxy server 
    When I send the WMS GetMap request
      | parameter | value           |
      | LAYERS    | EARTH:COVER     |
      | STYLES    |                 |
      | SRS       | EPSG:4326       |
      | FORMAT    | image/png       |
      | BBOX      | 10,10,10.1,10.1 |
      | WIDTH     | 4               |
      | HEIGHT    | 4               |
    Then I receive the image
      """
      # ImageMagick pixel enumeration: 4,4,255,srgb
      0,0: (14,14,14)  #0E0E0E  srgb(14,14,14)
      1,0: (14,14,14)  #0E0E0E  srgb(14,14,14)
      2,0: (20,20,20)  #141414  grey8
      3,0: (20,20,20)  #141414  grey8
      0,1: (20,20,20)  #141414  grey8
      1,1: (14,14,14)  #0E0E0E  srgb(14,14,14)
      2,1: (20,20,20)  #141414  grey8
      3,1: (20,20,20)  #141414  grey8
      0,2: (20,20,20)  #141414  grey8
      1,2: (20,20,20)  #141414  grey8
      2,2: (14,14,14)  #0E0E0E  srgb(14,14,14)
      3,2: (20,20,20)  #141414  grey8
      0,3: (20,20,20)  #141414  grey8
      1,3: (20,20,20)  #141414  grey8
      2,3: (14,14,14)  #0E0E0E  srgb(14,14,14)
      3,3: (20,20,20)  #141414  grey8
      """
