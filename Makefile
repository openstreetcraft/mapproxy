docker-compose.yml:
	wget https://gitlab.com/openstreetcraft/openstreetcraft-api/-/raw/master/docker-compose.yml

.PHONY: start
start: docker-compose.yml
	docker-compose up --build -d mapproxy

.PHONY: stop
stop: docker-compose.yml
	docker-compose down -v
